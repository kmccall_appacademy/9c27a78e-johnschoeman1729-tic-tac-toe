require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :player_one, :player_two
  attr_accessor :board, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = @player_one
  end

  def play
    puts "Welcome to Tic-Tac-Toe!"
    until @board.over?
      play_turn
    end
    conclude
  end

  def play_turn
    @current_player.display(@board)
    move = @current_player.get_move
    @board.place_mark(move, @current_player.mark)
    switch_players!
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

  def conclude
    puts "Game Over!"
    puts "#{@board.winner} has won!"
    @current_player.display(@board)
  end
end

if __FILE__ == $PROGRAM_NAME
  player_one = HumanPlayer.new("p1")
  player_two = ComputerPlayer.new("co")
  player_one.mark = :O
  player_two.mark = :X
  game = Game.new(player_one, player_two)
  game.play
end
