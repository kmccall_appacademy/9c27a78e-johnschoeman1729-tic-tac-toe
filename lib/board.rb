class Board
  attr_accessor :grid

  def initialize(grid = nil)
    if grid
      @grid = grid
    else
      @grid = Array.new(3) { Array.new(3) }
    end
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos] == nil
  end

  def winner
    # check rows
    @grid.each do |row|
      return row[0] if row.all? { |col| col == row[0] } && !row[0].nil?
    end
    # check cols
    @grid.transpose.each do |col|
      return col[0] if col.all? { |row| row == col[0] } && !col[0].nil?
    end
    # check diagonals
    tl, tr, mid, bl, br = [[0, 0], [0, 2], [1, 1], [2, 0], [2, 2]]
    return self[tl] if self[tl] == self[mid] && self[mid] == self[br]
    return self[tr] if self[tr] == self[mid] && self[mid] == self[bl]
    nil
  end

  def over?
    return true unless winner == nil
    return true if @grid.flatten.none? { |pos| pos == nil }
    false
  end

  def available_moves
    moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        moves << [row, col] if self[[row, col]].nil?
      end
    end
    moves
  end
end

def print_board(board)
  puts "---------"
  board.grid.each do |row|
    temp = ""
    row.each do |col|
      temp += col.nil? ? " - " : " #{col} "
    end
    puts temp
  end
  puts "---------"
end
