class ComputerPlayer
  attr_accessor :board, :mark
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
    print_board(board)
  end

  def get_move
    moves = @board.available_moves # test moves to find best move
    moves.each do |move|
      @board.place_mark(move, @mark)
      return move if board.winner == @mark
      @board[move] = nil
    end
    moves.sample
  end
end
