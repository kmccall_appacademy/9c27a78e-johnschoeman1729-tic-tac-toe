class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def get_move
    print "where "
    input = gets.chomp
    pos = input.split(', ').map!(&:to_i)
    pos
  end

  def display(board)
    print_board(board)
  end
end
